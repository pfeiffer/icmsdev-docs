
from subprocess import check_output

cmd = '' # 'virtualenv venv;'
cmd += 'pip install --upgrade pip;'
cmd += 'pip install -r requirements.txt;'
# cmd += 'source ./venv/bin/activate;'
cmd += 'echo `date`" -- Starting app ... ";'
cmd += 'FLASK_APP=hello.py flask run --host 0.0.0.0 --port 8080'
res = check_output(cmd, shell=True)

print (res)

